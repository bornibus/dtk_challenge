import os
import sqlite3
from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify
app = Flask(__name__)

# Upload folder for future .db files
UPLOAD_FOLDER = 'static/db'

# Get all the filenames in the db folder
def get_db_filename():
	database = []
	for f in os.listdir('static/db'):
		if '.db' == f[-3:]:
			database.append(f)
	return database

# Request for aggregation
def get_request(variable,agg,table):
	return 'select "{0}",count(*),round(avg({1}),2) from {2} where {1} is not null group by "{0}" order by round(avg({1}),2) desc'.format(variable,agg,table)

# Get the name of all the columns in 'table'
def get_column_table(table):
	return 'PRAGMA table_info({0});'.format(table)

# Get all tables from selected DB file
def get_tables_request():
	return 'select name from sqlite_master WHERE type="table";'

# Index.html - SPA
@app.route("/")
def index():
    return render_template('index.html')


# API

# get_databases - GET Method
# Get all the available db files
@app.route("/api/get_databases")
def get_databases():
	db = []
	database = get_db_filename()
	for index,value in enumerate(database):
		db.append({'id':index,'name':value})
	return jsonify({'databases':db})

# get_tables - GET Method
# Get all the tables containing in the db file
# with index data_index
@app.route("/api/get_tables/<int:database_index>")
def get_tables(database_index):
	database = get_db_filename()
	conn = sqlite3.connect('{0}/{1}'.format(UPLOAD_FOLDER,database[database_index]))
	c = conn.cursor()
	variable_list = []
	for i in c.execute(get_tables_request()):
		variable_list.append(i[0])
	return jsonify({'tables':variable_list})

# get_variable_list - GET Method
# Get all the varaibles containing in the table 'table'
# and in the db file with index data_index
# It splits the strings from the ints because we can only
# aggregate int (or float etc) variables
@app.route("/api/get_variable_list/<int:database_index>/<string:table>")
def get_variable_list(database_index,table):
	database = get_db_filename()
	conn = sqlite3.connect('{0}/{1}'.format(UPLOAD_FOLDER,database[database_index]))
	c = conn.cursor()
	variable_list = []
	for i in c.execute(get_column_table(table)):
		if 'int' in i[2]:
			variable_list.append({'name':i[1],'type':'int'})
		else:
			variable_list.append({'name':i[1],'type':'string'})
	return jsonify({'variables': variable_list})

# get_statistic - GET Method
# Compute the aggregation with the variable 'agg' grouped by the variable 'variable'
@app.route("/api/get_statistic/<int:database_index>/<string:table>/<string:variable>/<string:agg>")
def get_statistic(database_index,table,variable,agg):
	database = get_db_filename()
	conn = sqlite3.connect('{0}/{1}'.format(UPLOAD_FOLDER,database[database_index]))
	c = conn.cursor()
	agg_list = []
	ind = 0
	acc = 0
	for i in c.execute(get_request(variable,agg,table)):
		if ind < 100:
			agg_list.append({'variable':i[0],'count':i[1],'avg':i[2]})
		else:
			acc = acc + i[1]
		ind = ind + 1
	response = {'statistics':agg_list}
	if ind > 100:
		response['remaining_obs'] = ind - 100
	if acc > 0:
		response['remaining_rows'] = acc
	return jsonify(response)


# upload_file - POST Method
# Upload a new db file. It'll store under UPLOAD_FOLDER
@app.route('/api/upload_database', methods=['POST'])
def upload_file():
	response = {}
	file = request.files['file']
	if '.db' == file.filename[-3:]:
		file.save(os.path.join(UPLOAD_FOLDER, file.filename))
		response['message']  = "Successfully uploaded."
	return jsonify(response)

# Run the application
if __name__ == "__main__":
    app.run()
