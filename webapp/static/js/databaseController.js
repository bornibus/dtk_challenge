/**
 * databaseController.js
 */

dataikuApp.controller('databaseController', function($scope,$http) {
    $scope.message = "";
    $scope.error = "";
    $scope.progressCounter = "";
    
	$scope.fileSelected = function(element){
        var fileSelected = element.files[0];
        console.log(fileSelected);
        var fd = new FormData();
        fd.append('file',fileSelected);
        $http({
            method:'POST',
            url:'/api/upload_database',
            uploadEventHandlers: {
                progress: function (e) {
                    if (e.lengthComputable) {
                        $scope.progressBar = (e.loaded / e.total) * 100;
                        $scope.progressCounter = Math.round($scope.progressBar);
                    }
                }
            },
            data:fd,
            headers:{'Content-Type':undefined}
        }).success(function(data,status,headers,config){
            $scope.message = data['message'];
            $scope.progressCounter = "";

        }).error(function(data,status,headers,config){
            $scope.error = "Oops, something went wrong !";
            $scope.progressCounter = "";
        });
    };
});