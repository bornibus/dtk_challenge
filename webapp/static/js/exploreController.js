/**
 * exploreController.js
 */

dataikuApp.controller('exploreController', function($scope,$http) {
    $scope.rowCollection = [];
    $scope.loading = "";

	$http.get("/api/get_databases").then(function successCallback(response) {
        $scope.db = response.data;
    }, function errorCallback(response) {
    });

    $scope.selectDb = function(){
        $scope.variable = "";
        $scope.variables = "";
        $scope.table = "";
        $http.get("/api/get_tables/"+$scope.db.selected.id).then(function successCallback(response) {
            $scope.tables = response.data.tables;
        }, function errorCallback(response) {
    
        });
    };

    $scope.changeTable = function(){
        $scope.variable = "";
        $scope.variables = "";
        $http.get("/api/get_variable_list/"+$scope.db.selected.id+"/"+$scope.table).then(function successCallback(response) {
            $scope.variables = response.data.variables;
        }, function errorCallback(response) {
    
        });
    };

    $scope.changeVariable = function(){
        $scope.rowCollection = [];
    };

    $scope.filter = function(){
        $scope.loading = "loading";
        $http.get("/api/get_statistic/"+$scope.db.selected.id+"/"+$scope.table+"/"+$scope.groupByVariable.name+"/"+$scope.aggVariable.name).then(function successCallback(response) {
            $scope.rowCollection = response.data.statistics;
            $scope.remaining_rows = response.data.remaining_rows;
            $scope.remaining_obs = response.data.remaining_obs;
            $scope.loading = "";
        }, function errorCallback(response) {
            $scope.loading = "";
        });
    };
});
