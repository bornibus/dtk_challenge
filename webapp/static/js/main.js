/**
 * main.js
 */

var dataikuApp = angular.module('dataikuApp', ['ngMaterial','ngRoute']);
dataikuApp.config(['$locationProvider', function($locationProvider) {
  $locationProvider.hashPrefix('');
}]);

dataikuApp.config(function($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl : '../static/view/main.html',
        controller  : 'mainController'
    })
    .when('/explore', {
        templateUrl : '../static/view/explore.html',
        controller  : 'exploreController'
    })
    .when('/database', {
        templateUrl : '../static/view/database.html',
        controller  : 'databaseController'
    })
    .otherwise({redirectTo:'/'});
});