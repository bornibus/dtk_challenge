# Dataiku challenge #

### Configuration ###

* Use Python2.7
* Install flask : pip install flask
* Install sqlite3 : pip install sqlite3

### How do I launch the tool? ###

* Clone the repository : git clone https://bornibus@bitbucket.org/bornibus/dtk_challenge.git
* Open your terminal in dtk_challenge/webapp
* Run : python app.py
* Open your browser (not explorer, please !) and connect to http://localhost:5000
* Have fun ! Test it, break it and improve it ! 
